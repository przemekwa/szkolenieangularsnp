import { PlanetDetectorModule } from './planet-detector.module';

describe('PlanetDetectorModule', () => {
  let planetDetectorModule: PlanetDetectorModule;

  beforeEach(() => {
    planetDetectorModule = new PlanetDetectorModule();
  });

  it('should create an instance', () => {
    expect(planetDetectorModule).toBeTruthy();
  });
});
