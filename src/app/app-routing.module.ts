import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlackHoleComponent } from './black-hole/black-hole.component';

const routes: Routes = [
  {path: 'intel', loadChildren: 'src/app/intel/intel.module#IntelModule'},
  {path: 'photo-browser', loadChildren: 'src/app/photo-browser/photo-browser.module#PhotoBrowserModule'},
  {path: 'planet-detector', loadChildren: 'src/app/planet-detector/planet-detector.module#PlanetDetectorModule'},
  {path: 'radio', loadChildren: 'src/app/radio-decoder/radio-decoder.module#RadioDecoderModule'},
  {path: '', redirectTo: 'space', pathMatch: 'full'},
  {path: '**', component: BlackHoleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
