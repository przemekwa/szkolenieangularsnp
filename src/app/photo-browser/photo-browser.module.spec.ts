import { PhotoBrowserModule } from './photo-browser.module';

describe('PhotoBrowserModule', () => {
  let photoBrowserModule: PhotoBrowserModule;

  beforeEach(() => {
    photoBrowserModule = new PhotoBrowserModule();
  });

  it('should create an instance', () => {
    expect(photoBrowserModule).toBeTruthy();
  });
});
