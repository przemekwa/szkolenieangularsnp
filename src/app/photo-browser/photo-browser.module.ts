import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotoBrowserRoutingModule } from './photo-browser-routing.module';
import { PhotoListComponent } from './photo-list/photo-list.component';
import { PhotoFormComponent } from './photo-form/photo-form.component';
import { PhotoBrowserComponent } from './photo-browser/photo-browser.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PhotoBrowserRoutingModule,
    SharedModule
  ],
  declarations: [PhotoListComponent, PhotoFormComponent, PhotoBrowserComponent]
})
export class PhotoBrowserModule { }
