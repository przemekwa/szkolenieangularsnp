import { TestBed, inject } from '@angular/core/testing';

import { DecoderService } from './decoder.service';

describe('DecoderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DecoderService]
    });
  });

  it('should ...', inject([DecoderService], (service: DecoderService) => {
    expect(service).toBeTruthy();
  }));
});
